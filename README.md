## How To Run This Project? ##

1) Run Composer Install
```
composer install
```

2) Setup Database Environment in .env

3) Run Migration and Seeder
```
php artisan migrate:refresh --seed
```

4) Run Artisan Serve to See
```
php artisan serve
```

-------------

Account for Demo Purpose:
```
Email: admin@dtta.co
Password: admin123
```