<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lendings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('movie_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->date('lending_date');
            $table->date('returned_date')->nullable();
            $table->date('expected_returned_date');
            $table->integer('lateness_charge')->default(0);
            $table->integer('lateness_charge_per_day');
            $table->timestamps();

            $table->foreign('movie_id')
                  ->references('id')->on('movies')
                  ->onDelete('cascade');

            $table->foreign('member_id')
                  ->references('id')->on('members')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lendings');
    }
}
