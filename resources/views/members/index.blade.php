@extends('layouts.app')

@section('title', 'Members')

@section('content')
	<a href="{{ route('member.create') }}" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> Add New Member</a>
		<table id="member-table" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>#</td>
					<td>Name</td>
					<td>Age</td>
					<td>Address</td>
					<td>Telephone</td>
					<td>Identity Number</td>
					<td>Joined Date</td>
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				@foreach($members as $member)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $member->name }}</td>
						<td>{{ $member->age }}</td>
						<td>{{ $member->address }}</td>
						<td>{{ $member->telephone }}</td>
						<td>{{ $member->identity_number }}</td>
						<td>{{ $member->joined_date }}</td>
						<td>{{ $member->is_active == 1 ? 'Active' : 'Not Active' }}</td>
						<td>
							<a href="{{ route('member.edit', ['id' => $member->id]) }}"><i class="fa fa-edit"></i> Edit</a>
							|
							<a href="#" class="member-data" data-id="{{ $member->id }}"><i class="fa fa-trash"></i> Delete</a>
							|
							@if($member->is_active == 1)
								<a href="#" class="member-status" data-id="{{ $member->id }}">Deactived</a>
							@else
								<a href="#" class="member-status" data-id="{{ $member->id }}">Activated</a>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<form method="post" id="delete-member" action="">
			@csrf
			{{ method_field('delete') }}
		</form>

		<form method="post" id="change-member" action="">
			@csrf
			{{ method_field('put') }}
		</form>
@endsection

@section('extra-js')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#member-table').DataTable();

		    @if(session('success') == 'store')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been stored.',
				    icon: 'success'
		    	});
		    @endif

		    @if(session('success') == 'update')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been updated.',
				    icon: 'success'
		    	});
		    @endif

		    @if(session('success') == 'delete')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been deleted.',
				    icon: 'success'
		    	});
		    @endif

		    $(".member-data").on('click', function() {
		    	swal({
				  title: "Are you sure?",
				  text: "Once deleted, you will not be able to recover this data!",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
				  	var $id = $(this).data('id');
				    $("#delete-member").attr('action', "members/" + $id);
				    $("#delete-member").submit();
				  } else {

				  }
				});
		    });

		    $(".member-status").on('click', function() {
		    	var $id = $(this).data('id');
			    $("#change-member").attr('action', "members/" + $id);
			    $("#change-member").submit();
		    });
		} );
	</script>
@endsection