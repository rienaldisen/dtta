<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MovieRepository;
use Validator;

class IndexMovie extends Controller
{
	public function __invoke(MovieRepository $repo) {
		$movies = $repo->showAll();

		return view('movies.index', compact('movies'));
    }
}