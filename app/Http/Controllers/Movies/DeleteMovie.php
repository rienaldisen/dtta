<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MovieRepository;
use Validator;

class DeleteMovie extends Controller
{
	public function __invoke(MovieRepository $repo, $id) {
		$repo->delete($id);

        return redirect()->route('movie')->with('success', 'delete');
	}
}