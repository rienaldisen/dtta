<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;

class ChangeStatusMember extends Controller
{
	public function __invoke(MemberRepository $repo, $id) {
        $repo->change_status($id);
        return redirect()->route('member')->with('success', 'update');
    }
}