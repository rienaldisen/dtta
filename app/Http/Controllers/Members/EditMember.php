<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;

class EditMember extends Controller
{
	public function __invoke(MemberRepository $repo, $id) {
		$member = $repo->findOne($id);

		return view('members.edit', compact('member'));
	}
}