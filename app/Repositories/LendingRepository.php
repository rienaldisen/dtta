<?php

namespace App\Repositories;

use App\Lending;
use Carbon\Carbon;

class LendingRepository
{
    protected $model;

    public function __construct(Lending $lending)
    {
    	$this->model = $lending;
    }

    public function save($request)
    {
    	return $this->model->create($request);
    }

    public function showAllUnreturned()
    {
    	return $this->model->where('returned_date', null)->get();
    }

    public function return_lending($id)
    {
    	$lateness_charge = 0;
    	$data = $this->model->findOrFail($id);

    	if(Carbon::now() > $data->expected_returned_date)
    	{
    		$lateness_charge = Carbon::now()->diffInDays($data->expected_returned_date) * $data->lateness_charge_per_day;
    	}

    	return $data->update([
    		'returned_date' => Carbon::now(),
    		'lateness_charge' => $lateness_charge
    	]);
    }
}