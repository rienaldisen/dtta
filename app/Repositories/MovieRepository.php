<?php

namespace App\Repositories;

use App\Movie;

class MovieRepository
{
    protected $model;

    public function __construct(Movie $movie)
    {
        $this->model = $movie;
    }

    public function showAll()
    {
        return $this->model->all();
    }

    public function save($request)
    {
        return $this->model->create($request);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function edit($id, $request)
    {
    	return $this->model->find($id)->update($request);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

}