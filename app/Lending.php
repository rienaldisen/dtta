<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lending extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'movie_id',
        'member_id',
        'lending_date',
        'returned_date',
        'expected_returned_date',
        'lateness_charge',
        'lateness_charge_per_day'
    ];

    protected $dates = [
        'lending_date',
        'returned_date',
        'expected_returned_date',
    ];

    public function movie()
    {
        return $this->belongsTo('App\Movie', 'movie_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }
}